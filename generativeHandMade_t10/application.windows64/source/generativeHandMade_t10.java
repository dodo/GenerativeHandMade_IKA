import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import remixlab.bias.event.*; 
import remixlab.bias.event.shortcut.*; 
import remixlab.bias.agent.profile.*; 
import remixlab.bias.agent.*; 
import remixlab.dandelion.agent.*; 
import remixlab.proscene.*; 
import remixlab.dandelion.core.*; 
import remixlab.dandelion.constraint.*; 
import remixlab.fpstiming.*; 
import remixlab.util.*; 
import remixlab.dandelion.geom.*; 
import remixlab.bias.core.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class generativeHandMade_t10 extends PApplet {

//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//about sketching in a field

 //import proscene library













Scene scn;//define "scn"as Scene(part of proscene library)
ArrayList dots = new ArrayList();//create an empty list to store the particles (dots in this case, just the name..)
float nsc=0.05f;//noise scale, check processing help noise() function

public void setup(){
size(1000,1000,P3D);  
scn=new Scene(this);//create the scene
scn.setAxisVisualHint(false);//disable display of axes in the scene, refere to proscene html document, or to the examples that go with it
scn.setGridVisualHint(false);
scn.setRadius(1000);
for(int i=0;i<10;i++)dots.add(new Dot(new PVector(random(-10,10),random(-10,10),random(-10,10))));//create 10 new dots in a random place and add them to "dots" list
smooth(32);//turn on anti-aliasing, check processing help
}


public void draw(){
background(255); 
for(int i=0;i<dots.size();i++){//for all the elements of "dots" list
Dot d=(Dot)dots.get(i);//pick one
d.update();//call its internal "update" function
d.display();//call its internal "display" function
}
}


public void keyPressed(){
if(key=='q'&&nsc<0.5f)nsc+=0.001f;
if(key=='w'&&nsc>0.0001f)nsc-=0.001f;
println(nsc);
}




class Dot{
PVector pos;// define "pos"as a vector, check processing help for PVector description
InteractiveFrame f;//part of proscene library enabling interaction with mouse
ArrayList<PVector> path=new ArrayList<PVector>();// list of vectors storing the path, so that linework can be displayed on the screen
int t;//variable to limit the time when motion happens, check further down

Dot(PVector _pos){//making a new "Dot" requires providing a PVector of it initial position
pos=_pos;//which we assign to internal "pos" vector
f=new InteractiveFrame(scn);//
f.setPosition(pos.x,pos.y,pos.z);//set the position of interactive frame, so that it matches the position of the particle
t=0;// initial time is 0
}

public void update(){
  if(t<200){//only if t is smaller then 200 do the following:
PVector vel = new PVector(noise(1,pos.y*nsc,pos.z*nsc)-0.5f,noise(pos.x*nsc,2,pos.z*nsc)-0.5f,noise(pos.x*nsc,pos.y*nsc,3)-0.5f);//create a new vector out of noise data, check processing help for description of noise()
vel.mult(2);//multiply the velocity vector by 2
pos.add(vel);// add it to current position - this results in motion
if(frameCount%10==0)path.add(new PVector(pos.x,pos.y,pos.z));//every 10 frames, add current position to the path
f.setPosition(pos.x,pos.y,pos.z);//update the interactive frame, so that it matches 
t++;//update time by 1
  }
}


public void display(){  
strokeWeight(0.5f);  
  for(int i=1;i<path.size();i++){//for the whole length of the path
   PVector a=path.get(i-1); 
   PVector b=path.get(i); 
   line(a.x,a.y,a.z,b.x,b.y,b.z);//draw a line between adjacent points
  }  
strokeWeight(3);
stroke(0);
if(f.grabsInput(scn.mouseAgent())){//if mouse interaction is detected
 stroke(255,0,0);//change color to red
  if(keyPressed&&key==' '){//if additionally spacebar is pressed
   dots.add(new Dot(new PVector(pos.x+random(-10,10),pos.y+random(-10,10),pos.z+random(-10,10)))); //add a new dot somewhere around this one   
  }   
}
point(pos.x,pos.y,pos.z);//display current position as a point
}
  
  
}

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--stop-color=#cccccc", "generativeHandMade_t10" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
