//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//about sketching in a field

import remixlab.bias.event.*; //import proscene library
import remixlab.bias.event.shortcut.*;
import remixlab.bias.agent.profile.*;
import remixlab.bias.agent.*;
import remixlab.dandelion.agent.*;
import remixlab.proscene.*;
import remixlab.dandelion.core.*;
import remixlab.dandelion.constraint.*;
import remixlab.fpstiming.*;
import remixlab.util.*;
import remixlab.dandelion.geom.*;
import remixlab.bias.core.*;


Scene scn;//define "scn"as Scene(part of proscene library)
ArrayList dots = new ArrayList();//create an empty list to store the particles (dots in this case, just the name..)
float nsc=0.05;//noise scale, check processing help noise() function

void setup(){
size(1000,1000,P3D);  
scn=new Scene(this);//create the scene
scn.setAxisVisualHint(false);//disable display of axes in the scene, refere to proscene html document, or to the examples that go with it
scn.setGridVisualHint(false);
scn.setRadius(1000);
for(int i=0;i<10;i++)dots.add(new Dot(new PVector(random(-10,10),random(-10,10),random(-10,10))));//create 10 new dots in a random place and add them to "dots" list
smooth(32);//turn on anti-aliasing, check processing help
}


void draw(){
background(255); 
for(int i=0;i<dots.size();i++){//for all the elements of "dots" list
Dot d=(Dot)dots.get(i);//pick one
d.update();//call its internal "update" function
d.display();//call its internal "display" function
}
}


void keyPressed(){
if(key=='q'&&nsc<0.5)nsc+=0.001;
if(key=='w'&&nsc>0.0001)nsc-=0.001;
println(nsc);
}




