//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//basic movement+extracting data from a picture

//global variables:
ArrayList<PVector> pts = new ArrayList<PVector>();//create an empty list for PVectors and call it "pts"
boolean rec=false;//create a boolean variable to toggle recording animation, and set it to false
PImage bckg;//create a variable to store a picture

void setup() {
  bckg=loadImage("miro_poetesse40.jpg");//load a picture. it needs to be in the "data"folder of this sketch
  size(1300, 1063);//adjust the resolution of this app to the picture  
  image(bckg, 0, 0);//display the picture
  smooth(32);
}

void draw() {
  for (PVector p : pts) {//for each PVector in the "pts" list, call it "p" for now.. 
    color c=bckg.get(round(p.x), round(p.y));//store the color of the pixel below the point under name 'c' for further use:
    fill(255-red(c), 255-green(c), blue(c), 150);//change the color according to what is the pixel value below, notice that red and green channels are inverted
    //local variables:
    float s=map(red(c), 0, 255, 2, 40);//create a variable "s" and store under it value of the red channel of the picture mapped from 0-255 to 2-40
    ellipse(p.x, p.y, s, s);//use the above variable for the size of the ellipse
    float r=map(blue(c), 0, 255, 1, 10);//    
    p.add(new PVector(random(-r, r), random(-r, r+1)));//use the above variable to change the range of random motion according to the picture blue channel, check PVector's "add" method in processing reference
  }
  if (rec==true) {//only if "rec" is true
    saveFrame("ani/frm-####.jpg");//save a frame of animation into the folder "ani"
  }
}
void mouseDragged() {//when mouse s dragged...
  pts.add(new PVector(mouseX, mouseY));//..add a new vector on the current mouse position to the "pts" list
}
void keyPressed() {//when a key is pressed (any)..
  rec=!rec;//toggle the rec variable (switch from true to false and back again)
}