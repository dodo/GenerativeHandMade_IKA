//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//more elaborated version of tutorial 06 - similar structure, but introducing toxiclibs verletphysics library for particle-spring simulation

import toxi.geom.*;
import toxi.math.*;
import toxi.util.*;
import toxi.physics.*;
import toxi.physics.behaviors.*;

import remixlab.bias.event.*;
import remixlab.bias.event.shortcut.*;
import remixlab.bias.agent.profile.*;
import remixlab.bias.agent.*;
import remixlab.dandelion.agent.*;
import remixlab.proscene.*;
import remixlab.dandelion.core.*;
import remixlab.dandelion.constraint.*;
import remixlab.fpstiming.*;
import remixlab.util.*;
import remixlab.dandelion.geom.*;
import remixlab.bias.core.*;

Scene scn;
//two lines below are part of verletphysics library, compare with examples
VerletPhysics world;
GravityBehavior g;

ArrayList nodes = new ArrayList();

boolean update=false;//we will update the physics simulation only when this variable will be toggled to 'true' by the keypress, check below

void setup() {
  size(1000, 1000, P3D);
  scn=new Scene(this);
  scn.setAxesVisualHint(false);
  
  world=new VerletPhysics();//'world' is the name of our physics simulation, check inside of the node class, that anything that we want to simulate needs to be 'added' to the 'world'
  world.setDrag(0.033);
  
  g=new GravityBehavior(new Vec3D(0, 0, -0.01));//defining the gravity force, we can add or remove with keyPressed function below
  
  for (int i=0; i<10; i++)nodes.add(new Node(new Vec3D(random(-100, 100), random(-100, 100), 0)).lock());//add 10 nodes and lock them (part of verletParticle properties, check html file in verletphysics folder)
  
  smooth(32);
}


void draw() {
   background(255);
   
  if(update)world.update();//update the simulation only when 'update' variable is true

  for (int i=0; i<nodes.size (); i++) {
    Node n=(Node)nodes.get(i);
    n.display();
  }    
}

void keyPressed(){
if(key=='u')update=!update;  
if(key=='q')world.addBehavior(g); 
if(key=='w')world.removeBehavior(g); 
}
  



