class Node extends VerletParticle {// here is the important part - we are building on top of VerletParticle class from toxiclibs: https://www.processing.org/reference/extends.html 
  InteractiveFrame iframe;//this is the same as in tutorial 06
  AttractionBehavior iRep;//part of verletphysics, an attraction/repulsion force
  
  ArrayList connections = new ArrayList();//there is an internal list of connections 

  Node(Vec3D _pos) {
    super(_pos);//this line means, that we use our own '_pos' variable instead of VerletParticle internal stuff, it's a usefull technique: https://www.processing.org/reference/extends.html
    iframe=new InteractiveFrame(scn);
    iframe.setPosition(this.x, this.y, this.z);
    
    world.addParticle(this);//make sure that this particle is part of our simulation
    iRep=new AttractionBehavior(this, 10, -0.02f);//add a repulsion (-0.02) force around the particle (radius of 10), change these values for different behaviors
    world.addBehavior(iRep);//make sure that the above behavior is part of the simulation

    for (int i=0; i<nodes.size (); i++) {//the sequence below connects nodes within the radius of 20 to the one currently created with springs...
      Node cn=(Node)nodes.get(i);
      if (cn.distanceTo(this)<20&&cn!=this) {
        VerletSpring conn=new VerletConstrainedSpring(this, cn, 10, 0.001);//..springs will have length of 10 and stiffness of 0.001
        connections.add(conn);
        world.addSpring(conn);
      }
    }
  }


  void display() {
    strokeWeight(7);
    stroke(0);
    if (iframe.grabsInput(scn.mouseAgent())) {
      stroke(255, 0, 0);
      strokeWeight(11);
      if (keyPressed&&key==' ')nodes.add(new Node(new Vec3D(this.x+random(-10, 10), this.y+random(-10, 10), this.z+random(-10, 10))));
    }
    iframe.setPosition(this.x, this.y, this.z);//since the node is moving according to the simulation, we need to constantly update the interactive frame, so that we can still interact with it!
    point(this.x, this.y, this.z);
    strokeWeight(0.2);
    stroke(0);
    for (int i=0; i<connections.size (); i++) {//draw all the connecting spring as lines
      VerletSpring c=(VerletSpring)connections.get(i);
      line(c.a.x, c.a.y, c.a.z, c.b.x, c.b.y, c.b.z);
    }
  }
}

