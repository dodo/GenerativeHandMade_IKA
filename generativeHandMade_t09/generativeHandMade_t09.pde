//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//how to create a volume around a line drawing and change properties of it

import toxi.processing.*;
import toxi.volume.*;
import toxi.math.conversion.*;
import toxi.geom.*;
import toxi.math.*;
import toxi.geom.mesh2d.*;
import toxi.util.datatypes.*;
import toxi.util.events.*;
import toxi.geom.mesh.subdiv.*;
import toxi.geom.mesh.*;
import toxi.math.waves.*;
import toxi.util.*;
import toxi.math.noise.*;
import toxi.physics.*;
import toxi.physics.constraints.*;
import toxi.physics.behaviors.*;
import remixlab.bias.event.*;
import remixlab.bias.event.shortcut.*;
import remixlab.bias.agent.profile.*;
import remixlab.bias.agent.*;
import remixlab.dandelion.agent.*;
import remixlab.proscene.*;
import remixlab.dandelion.core.*;
import remixlab.dandelion.constraint.*;
import remixlab.fpstiming.*;
import remixlab.util.*;
import remixlab.dandelion.geom.*;
import remixlab.bias.core.*;

Scene scn;

ArrayList nodes = new ArrayList();
ArrayList fibres = new ArrayList();
ArrayList selected = new ArrayList();

VerletPhysics world;
GravityBehavior g;

boolean showMesh=true;
boolean update=true;

//everything below referes to the volume, check previous tutorials and volumeutils examples
int dim =150;
int sc=1000;
Vec3D scl = new Vec3D(sc, sc, sc);
float thrs=0.5;
VolumetricSpaceArray vol;
IsoSurface srf;
WETriangleMesh m;
VolumetricBrush brush;
/////////////////////
ToxiclibsSupport gfx;

void setup() {
  size(1000, 1000, P3D);
  
  scn=new Scene(this);
  scn.setRadius(1000);
  scn.setAxesVisualHint(false);

  world=new VerletPhysics();
  world.setDrag(0.01);   

  g = new GravityBehavior(new Vec3D(0, 0, 0.01));

  for (int i=0; i<50; i++)nodes.add(new Node(new Vec3D(random(-100, 100), random(-100, 100), random(-100, 100))).lock());
  makeVolume();//check 'vol' tab

  gfx=new ToxiclibsSupport(this);
  smooth(32);
}


void draw() {
  background(255);
  if (update)world.update();

  for (int i=0; i<nodes.size (); i++) {//display all the nodes
    Node n=(Node)nodes.get(i);
    n.display();
  }

  for (int i=0; i<fibres.size (); i++) {//display all the fibres
    Fibre f=(Fibre)fibres.get(i);
    f.display();
  }
  strokeWeight(0.5);
  lights();
  if (showMesh)gfx.mesh(m);//display the mesh
}

