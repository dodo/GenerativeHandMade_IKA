class Fibre {//each fibre has a beginning and end vector as well as a list of its nodes
  
  Node bg, fin;
  ArrayList<Node> myNodes;

  Fibre(Node _bg, Node _fin) {//creating a fibre means:
    bg=_bg;
    fin=_fin;
    myNodes=new ArrayList<Node>();
    Line3D l = new Line3D(bg, fin);//computing a straight line between beginning and end...
    ArrayList<Vec3D> pts=new ArrayList<Vec3D>();
    l.splitIntoSegments(pts, 7, false);//..splitting the line into segments (arbitrary length, change the value for different densities)

    myNodes.add(bg);
    for (Vec3D p : pts) {//for each split-point..
      Node nu=new Node(p);//..add a new node
      world.addBehavior(new AttractionBehavior(nu,100,-0.01));//..and a repulsion behavior attached to it
      nodes.add(nu);
      myNodes.add(nu);
    }
    myNodes.add(fin);
    
    for (int i=0; i<myNodes.size ()-1; i++) {//connect all the nodes of the fibre with springs, one after the other
      Node a=myNodes.get(i);
      Node b=myNodes.get(i+1);
      world.addSpring(new VerletSpring(a, b, 10, 0.001));
    }
  }

  void display() {//display the fibre as a sequence of lines connecting all of it's nodes
    strokeWeight(1);
    for (int i=0; i<myNodes.size ()-1; i++) {
      Node a=myNodes.get(i);
      Node b=myNodes.get(i+1);
      line(a.x, a.y, a.z, b.x, b.y, b.z);
    }
  }
}

