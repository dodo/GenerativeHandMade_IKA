void makeVolume() {
  vol=new VolumetricSpaceArray(scl, dim, dim, dim);
  brush=new RoundBrush(vol, 1);
  m=new WETriangleMesh();
  InterpolateStrategy linear=new LinearInterpolation();//this is part of toxiclibscore library, it just helps finding values between given ones... check interpolate example 

//here is the tricky bit:
  for (int i=0; i<world.springs.size(); i++) {//for all the springs
    VerletSpring s=world.springs.get(i);
    Node bg=(Node)s.a;//take beginning..
    Node fin=(Node)s.b;//and end..
    Line3D l = new Line3D(bg, fin);//draw a line
    ArrayList<Vec3D> pts=new ArrayList<Vec3D>();
    l.splitIntoSegments(pts, 0.5, false);//and split it
    for (int j=0;j<pts.size();j++) {//for each split point along the line
      Vec3D p=pts.get(j);
      brush.setSize(pow(linear.interpolate(bg.s, fin.s, map(j, 0, pts.size(), 0, 1)), 2)/2);//adjust the size of volumetric brush (check volumetricBrush example from volumeutils)
      brush.drawAtAbsolutePos(p, 0.5);//and draw along the line
    }
  }
  vol.closeSides();
  srf=new ArrayIsoSurface(vol);
  srf.computeSurfaceMesh(m, thrs);
}

