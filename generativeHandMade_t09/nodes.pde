class Node extends VerletParticle {//node is rather simple extension of a verletParticle, like in previous tutorials

  InteractiveFrame f;
  float s;//note, that each node has certain size

  Node(Vec3D pos) {
    super(pos);  
    f=new InteractiveFrame(scn);
    f.setPosition(this.x, this.y, this.z);
    world.addParticle(this);
    s=random(3,7);//the size is random at first
  }

  void display() {
    f.setPosition(this.x, this.y, this.z);
    strokeWeight(pow(s,2)/3);
    
    stroke(0);
    if(f.grabsInput(scn.mouseAgent())){//but when we point at a node..
      stroke(255,0,0);
      if(keyPressed&&key=='x'&&s<20)s+=0.1;//..and press x or z, we can change the size
      if(keyPressed&&key=='z'&&s>1)s-=0.1;
    }
    if(selected.contains(this))stroke(100,255,0);
    point(this.x, this.y, this.z);
  }
}

