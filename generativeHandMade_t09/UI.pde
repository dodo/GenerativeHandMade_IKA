void keyPressed() {//usual user interface
  if(key=='v'){
   makeVolume(); 
  }
  if (key == 'l') {
    new LaplacianSmooth().filter(m, 1);
  }
      if (key == 's') {//save it as stl
     m.saveAsSTL(sketchPath("volume_"+month()+day()+hour()+minute()+".stl"));
  }
  if(key=='m')showMesh=!showMesh;
  if(key=='u')update=!update;
  if (key=='q')world.addBehavior(g);
  if (key=='w')world.removeBehavior(g);
}

void mouseReleased() {//here we are drawing new fibres!
  for (int i=0; i<nodes.size (); i++) {//for all the nodes
    Node n=(Node)nodes.get(i);//pick one  
    if (n.f.grabsInput(scn.mouseAgent())) {//and if it is below the mouse pointer
      selected.add(n); //add it to selected 
      if (selected.size()==2) {//...and if there are two selected
        Node bg=(Node)selected.get(0);//take the first
        Node fin=(Node)selected.get(1);//and the second
        fibres.add(new Fibre(bg, fin));//and connect them as a fibre
        selected.clear();//clear the selection
      }
    }
  }
}
