//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//basic 2D drawing functions
//all of the commands used here are explained in the processing help file

void setup() {
  size(1920, 1080); 
  background(255);
  smooth(32);
}

void draw() {//notice, that there is nothing in the draw() function, we simply never refresh the canvas, just keep adding to it...
}

void mouseDragged() {
  if (mouseButton==LEFT) {//draw point and a line
    strokeWeight(mouseX*0.01);
    stroke(100-sin(frameCount*0.1)*125,100);
    point(mouseX, mouseY);
    strokeWeight(mouseY*0.01);
    line(mouseX, mouseY, random(mouseX-70, mouseX+70), random(mouseY-70, mouseY+70));
  } 

  if (mouseButton==RIGHT) {//spread random points
    float s=(height-mouseY)*0.05;
    float spr=(width-mouseX)*0.2;
    strokeWeight(random(s));
    stroke(random(200),random(200));
    point(mouseX+random(-spr, spr), mouseY+random(-spr, spr));
  } 


  if (mouseButton==CENTER) {//draw white rectangle with low opacity
    rectMode(CENTER);
    noStroke();
    fill(255, 5);
    rect(mouseX, mouseY, random(200), random(200));
  }
}


void keyPressed() {
  if (key=='q') {//shuffle pixels around the mouse position
    for(int i=0;i<100;i++)set(mouseX+(int)random(-30,30), mouseY+(int)random(-30,30), get(mouseX, mouseY));
    
  } 
  if (key=='w') {//shift pixels downwards
    for (int y=height; y>1; y--) {
      set(mouseX, y, get(mouseX, y-1));
    }
  }

}











