void initGui() {//create sliders for our 3 variables, check examples from the library for more options
  cp5.addSlider("nscale")//fortunately commands of ControlP5 library are self-explanatory
    .setPosition(50, 50)
      .setRange(0.01, 0.05)
        ;
  cp5.addSlider("freq")
    .setPosition(50, 70)
      .setRange(20, 350)
        ;
  cp5.addSlider("floorFreq")
    .setPosition(50, 90)
      .setRange(1, dim)
        ;
  cp5.setAutoDraw(false);
}


void gui() {//draw 2d elements (sliders) on top of 3d graphics..
  hint(DISABLE_DEPTH_TEST);
  cam.beginHUD();
  cp5.draw();
  cam.endHUD();
  hint(ENABLE_DEPTH_TEST);
}


void buildSurface() {//here is the core of this sketch:

  VolumetricSpace volume=new VolumetricSpaceArray(scl, dim, dim, dim);//create new empty array of voxels of given scale (scl) and dimentions (dim)

  for (int z=0; z<dim; z++) {//for the three dimentions of the voxel space...
    for (int y=0; y<dim; y++) {
      for (int x=0; x<dim; x++) {

        //assign values between 0 and 1 to each voxel:
        if (y%floorFreq==0)volume.setVoxelAt(x, y, z, 1);// y%floorFreq==0 means "if a reminder of y divided by floorFreq equals 0", check "modulo" in processing help for details of this useful function
        else {
          volume.setVoxelAt(x, y, z, map(sin(noise(x*nscale, y*nscale, z*nscale)*freq), -1, 1, 0, 1));// fill volume with sin function of noise, you can place any mathematical formula here
        }
      }
    }
  }
  volume.closeSides();
  // store in IsoSurface and compute surface mesh for the given threshold value
  mesh=new WETriangleMesh();
  surface=new HashIsoSurface(volume);
  surface.computeSurfaceMesh(mesh, iso_thrs);
}

