//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//more about libraries + geometry and about taking parts of other codes
//exploration of smooth and striated space

import controlP5.*;// this library will allow creation and use of graphical user interface (sliders), check examples that go with it for possible applications, there are many!

import toxi.geom.*;//these will do the volume, the mesh and lots of other things, check the examples!
import toxi.geom.mesh.*;
import toxi.volume.*;
import toxi.math.noise.*;
import toxi.processing.*;

import peasy.test.*;
import peasy.org.apache.commons.math.*;
import peasy.*;
import peasy.org.apache.commons.math.geometry.*;

PeasyCam cam;//self explanatory
ControlP5 cp5;//under ths name will be our graphical user interface (sliders) from ControlP5 library, check "void initGui()" in functions tab for further operations
ToxiclibsSupport gfx;//this handles more complex drawing functions - in this case it will draw all the triangles of our mesh, without us having to write it

//we are going to use marching cubes algorithm provided by toxiclibs, google "marching cubes" to understand what it does..
IsoSurface surface;//these are part of volumeutils library, check other examples
WETriangleMesh mesh;

int dim=50;//resolution of the voxel matrix 
float iso_thrs = 0.5;//threshold defining what is solid and what is void
Vec3D scl=new Vec3D(300, 300, 300);

//these are the variables controlling different parameters of the volume, they are updated automatically with values from gui. notice, that while creating sliders in th initGui function, we are using these names too
float nscale=0.01;
float freq=70;
int floorFreq=10;


void setup() {
  size(1000, 1000, P3D); 
  cam=new PeasyCam(this, 500);
  cp5=new ControlP5(this);
  gfx=new ToxiclibsSupport(this);
  
  initGui();//initiates the GUI, check "functions" tab
  buildSurface();//check "functions" tab

  smooth(32);
}

void draw() {
  background(200);
  lights();
  strokeWeight(0.5);
  noFill();
  box(300, 300, 300);
  fill(230);
  strokeWeight(0.1);
  gfx.mesh(mesh);//display the mesh
  noLights();
  gui();//display the GUI, check "functions" tab 
}



void keyPressed() {
  if (key==' ') {
    buildSurface();//once you adjust the sliders, hit spacebar to update the volume
  }
  if (key == 'l') {//smooth the volume
    new LaplacianSmooth().filter(mesh, 1);
  }
    if (key == 's') {//save it as stl
     mesh.saveAsSTL(sketchPath("volume_"+month()+day()+hour()+minute()+".stl"));//please notice, that our filename will have current month, day hour and minute in it, check processing help for explanation of each particular function
  }
}

