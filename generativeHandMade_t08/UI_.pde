void keyPressed() {
  if (key=='q')world.addBehavior(g); 
  if (key=='w')world.removeBehavior(g);
  if (key==' ')update=!update;//toggle physics simulation
}

void mouseReleased() {
  if (mouseButton==LEFT) {//clicking left button adds a new dot and connects it with a spring to the one we clicked
    for (int i=0; i<dots.size (); i++) {
      Dot p=(Dot)dots.get(i);
      if (p.f.grabsInput(scn.mouseAgent())) {
        Dot n=new Dot(p.x+random(-10, 10), p.y+random(-10, 10), p.z+random(-10, 10)); 
        VerletSpring s=new VerletSpring(p, n, 10, 0.001);
        world.addSpring(s);
        spr.add(s);
      }
    }
  }
  if (mouseButton==CENTER) {//this will lock/unlock a given dot
    for (int i=0; i<dots.size (); i++) {
      Dot p=(Dot)dots.get(i);
      if (p.f.grabsInput(scn.mouseAgent())) {
        if (!p.isLocked())p.lock();
        else {
          p.unlock();
        }
      }
    }
  }
  if (mouseButton==RIGHT) {//this will add the clicked dot to the "selected" list, once there are 2 dots selected, it will connect them with a spring
    for (int i=0; i<dots.size (); i++) {
      Dot p=(Dot)dots.get(i);
      if (p.f.grabsInput(scn.mouseAgent())) {
        sel.add(p);
        if (sel.size()==2) {
          Dot a=(Dot)sel.get(0); 
          Dot b=(Dot)sel.get(1); 
          VerletSpring s=new VerletSpring(a, b, 10, 0.001);
          world.addSpring(s);
          spr.add(s);
          sel.clear();
        }
      }
    }
  }
}

