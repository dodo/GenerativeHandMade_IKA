class Dot extends VerletParticle {//Dot is actually a verletParticle (check examples) with added functionality, namely it has an interactiveFrame enabling mouse interaction

  InteractiveFrame f;

  Dot(float _x, float _y, float _z) {
    super(_x, _y, _z); 

    AttractionBehavior r=new AttractionBehavior(this, 50, -0.005);
    dots.add(this);

    world.addParticle(this);
    world.addBehavior(r);

    f=new InteractiveFrame(scn);
    f.setPosition(this.x, this.y, this.z);
  }

  void display() {
    f.setPosition(this.x, this.y, this.z);//it's important to update position of interactive frame, since the dot is moving according to simulation!
    stroke(0);
    if (this.isLocked())stroke(0, 0, 200);
    if (sel.contains(this))stroke(100, 200, 50);
    if (f.grabsInput(scn.mouseAgent())) {
      stroke(255, 0, 0);
    }

    point(this.x, this.y, this.z);
  }
}

