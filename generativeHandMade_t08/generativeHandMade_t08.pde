//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//building on top of libraries code by using extend command //https://www.processing.org/reference/extends.html
//check UI tab for all the interaction possibilities

import toxi.math.conversion.*;
import toxi.geom.*;
import toxi.math.*;
import toxi.util.datatypes.*;
import toxi.util.events.*;
import toxi.geom.mesh.subdiv.*;
import toxi.geom.mesh.*;
import toxi.math.waves.*;
import toxi.util.*;
import toxi.math.noise.*;
import toxi.physics.*;
import toxi.physics.constraints.*;
import toxi.physics.behaviors.*;

import remixlab.bias.event.*;
import remixlab.bias.event.shortcut.*;
import remixlab.bias.agent.profile.*;
import remixlab.bias.agent.*;
import remixlab.dandelion.agent.*;
import remixlab.proscene.*;
import remixlab.dandelion.core.*;
import remixlab.dandelion.constraint.*;
import remixlab.fpstiming.*;
import remixlab.util.*;
import remixlab.dandelion.geom.*;
import remixlab.bias.core.*;

Scene scn;

VerletPhysics world;
GravityBehavior g;

ArrayList dots=new ArrayList();//list of all the dots
ArrayList spr=new ArrayList();//list of all the springs
ArrayList sel=new ArrayList();//list of all the selected objects

boolean update=true;

void setup() {
  size(1000, 1000, P3D);

  scn=new Scene(this);
  scn.setRadius(1000);
  scn.setAxesVisualHint(false);

  world= new VerletPhysics();
  world.setDrag(0.005);

  g = new GravityBehavior(new Vec3D(0, 0, 0.01));

  for (int i=0; i<100; i++) {
    Dot p=new Dot(random(-50, 50), random(-50, 50), random(-50, 50)); //add 100 dots
  }
  for (int i=0; i<dots.size (); i++) {//connect randomly picked dots with springs
    Dot p=(Dot)dots.get(i);
    Dot b=(Dot)dots.get((int)random(dots.size()));
    VerletSpring s=new VerletSpring(p, b, 10, 0.001);
    world.addSpring(s);
    spr.add(s);
  }
}

void draw() {
  background(255);

  if (update)world.update();

  strokeWeight(10);
  for (int i=0; i<dots.size (); i++) {
    Dot p=(Dot)dots.get(i);
    p.display();
  }

  strokeWeight(1);
  for (int i=0; i<spr.size (); i++) {
    VerletSpring s=(VerletSpring)spr.get(i);
    line(s.a.x, s.a.y, s.a.z, s.b.x, s.b.y, s.b.z);
  }
}


