class Dot {
  Vec3D pos;// define "pos"as a vector, check processing help for PVector description
  InteractiveFrame f;//part of proscene library enabling interaction with mouse
  ArrayList<Vec3D> path=new ArrayList<Vec3D>();// list of vectors storing the path, so that linework can be displayed on the screen
  int t;//variable to limit the time when motion happens, check further down

  Dot(Vec3D _pos) {//making a new "Dot" requires providing a PVector of it initial position
    pos=_pos;//which we assign to internal "pos" vector
    f=new InteractiveFrame(scn);//
    f.setPosition(pos.x, pos.y, pos.z);//set the position of interactive frame, so that it matches the position of the particle
    t=0;// initial time is 0
  }

  void update() {
    if (t<200) {//only if t is smaller then 200 do the following:
      Vec3D vel = new Vec3D(noise(1, pos.y*nsc, pos.z*nsc)-0.5, noise(pos.x*nsc, 2, pos.z*nsc)-0.5, noise(pos.x*nsc, pos.y*nsc, 3)-0.5);//create a new vector out of noise data, check processing help for description of noise()
      vel.scaleSelf(2);//multiply the velocity vector by 2
      pos.addSelf(vel);// add it to current position - this results in motion
      if (frameCount%10==0)path.add(new Vec3D(pos.x, pos.y, pos.z));//every 10 frames, add current position to the path
      f.setPosition(pos.x, pos.y, pos.z);//update the interactive frame, so that it matches 
      t++;//update time by 1
    }
  }


  void display() {  
    strokeWeight(0.5);  
    for (int i=1;i<path.size();i++) {//for the whole length of the path
      Vec3D a=path.get(i-1); 
      Vec3D b=path.get(i); 
      line(a.x, a.y, a.z, b.x, b.y, b.z);//draw a line between adjacent points
    }  
    strokeWeight(3);
    stroke(0);
    if (f.grabsInput(scn.mouseAgent())) {//if mouse interaction is detected
      stroke(255, 0, 0);//change color to red
      if (keyPressed&&key==' ') {//if additionally spacebar is pressed
        dots.add(new Dot(new Vec3D(pos.x+random(-10, 10), pos.y+random(-10, 10), pos.z+random(-10, 10)))); //add a new dot somewhere around this one
      }
    }
    point(pos.x, pos.y, pos.z);//display current position as a point
  }
}

