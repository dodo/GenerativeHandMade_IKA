class Particle {//particle class almost identical to the one from previous tutorial

  PVector pos;
  PVector vel;
  PVector acc;
  float s;

  Particle() {
    pos=new PVector(random(-100, 100), 200, random(-100, 100));
    vel=new PVector(random(-1, 1), random(-1), random(-1, 1));
    s=random(3, 15);
  }

  void behave() {
    acc=PVector.sub(tar, pos);
    acc.normalize();
    acc.mult(s*0.001);
    vel.add(acc);
    //vel.mult(0.99);
    vel.limit(1);
    pos.add(vel);
  }

  void display() {
    stroke(0);
    strokeWeight(s);
    point(pos.x, pos.y, pos.z);
  }

  void connect() {
    for (Particle p: pts) {
      if (p.pos.dist(pos)<200&&p!=this)cn.add(new Connection(p.pos, this.pos));//check for any particles closer than 200, if there are any add a new Connection by providing two positions
    }
  }
}

