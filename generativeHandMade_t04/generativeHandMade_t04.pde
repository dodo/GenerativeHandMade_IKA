//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//intro to libraries and 3d, drawing a tower

import peasy.test.*;//importing the "peasycam" library. libraries are packages of classes and functions, they usually come with a bunch of examples and an html document with all the functions explained
import peasy.org.apache.commons.math.*;
import peasy.*;
import peasy.org.apache.commons.math.geometry.*;


ArrayList<Particle> pts = new ArrayList<Particle>();//create and empty list of particle objects described by the "Particle" class
ArrayList<Connection> cn=new ArrayList<Connection>();//create and empty list of connection objects described by the "Connection" class

PeasyCam cam;//use "cam" for whatever PeasyCam class from the library describes

int h=150;//initial height of the target, that will be followed by the particles
PVector tar=new PVector(random(-150, 150), h, random(-150, 150));// create the target at the above height, somewhere within 150 range from the center of the scene


void setup() {
  size(1000, 1000, P3D);//note the "P3D" part, check it in the processing reference file
  
  cam=new PeasyCam(this, 300);// create a new PeasyCam object attached to "this" sketch initially 300 units away from the center of the scene, check PeasyCam example and html doc for more info
  
  smooth(64);
  for (int i=0; i<20; i++)pts.add(new Particle());//create 20 Particle objects and add them to the pts list
}

void draw() {
  background(255); 
  for (Connection c : cn) c.display();//display all the connections
  for (Particle p : pts) {//make all the particles behave, and display them
    p.behave();
    p.display();
  }
  //display the target as a big red dot
  stroke(255,0,0);
  strokeWeight(15);
  point(tar.x,tar.y,tar.z);
  
  if (frameCount%10==0)for (Particle p : pts)p.connect();//every 10 frames of the animation call the connect() function of each particle
}


void keyPressed() {
  if (key==' ') {//whenever spacebar is pressed..
    h-=50;//update the height of the target
    tar=new PVector(random(-100, 100), h, random(-100, 100));
  }
}