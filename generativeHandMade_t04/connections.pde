class Connection {// this class describes the connections between particles, "connect" function inside of the "Particle" class creates new Connection objects 

  PVector bg, fn;//a connection needs a beginning and end vector

  Connection(PVector p1, PVector p2) {// in order to create a connection, we need to provide two vectors... check "connect" function inside of the "Particle" class
    bg=p1.get();//check PVector methods in the reference for the get() function
    fn=p2.get();
  }

  void display() {
    stroke(0, 100);
    strokeWeight(0.3);
    line(bg.x, bg.y, bg.z, fn.x, fn.y, fn.z);//draw a line between beginning and the end of the connection
  }
}

