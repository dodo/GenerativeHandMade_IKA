class Dot {//dot is a little more complicated, mostly because it interacts with the mouse
  InteractiveAvatarFrame frame;//this is part of proscene library - check the examples of mouse interaction
  Vec3D pos;//the dot is somewhere

  Dot(Vec3D _pos) {
    pos=_pos;
    frame = new InteractiveAvatarFrame(scn);  
    frame.setPosition(new Vec(pos.x, pos.y, pos.z));//make sure, that the clickable "interactive frame" and the actual position of the dot match!
  }  

  void display() {
    strokeWeight(s);//size of the dot according to the sliders
    stroke(0);//the dot is usually black
    if (frame.grabsInput(scn.motionAgent())) {//only if the mouse is above it..
      stroke(255, 0, 0);//..it turns red
      if (keyPressed&&key==' ') {//if additionally the spacebar is pressed
        Dot ndot=new Dot(new Vec3D(this.pos.x+random(-spread, spread), this.pos.y+random(-spread, spread), this.pos.z+random(-spread, spread)));//make a new dot somewhere around this one within spread radius
        allDots.add(ndot);//add it to the allDots list
        connect(ndot);//connect it to the other dots, check "void connect()" in functions tab        
      }
    }
    point(pos.x, pos.y, pos.z);
  }
}

