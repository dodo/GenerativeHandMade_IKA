//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//mouse-objects interaction, proscene library

import controlP5.*;
import toxi.geom.*;
import remixlab.bias.event.*;
import remixlab.bias.event.shortcut.*;
import remixlab.bias.agent.profile.*;
import remixlab.bias.agent.*;
import remixlab.dandelion.agent.*;
import remixlab.proscene.*;
import remixlab.dandelion.core.*;
import remixlab.dandelion.constraint.*;
import remixlab.fpstiming.*;
import remixlab.util.*;
import remixlab.dandelion.geom.*;
import remixlab.bias.core.*;

Scene scn;
ControlP5 gui;

ArrayList allDots = new ArrayList();//empty list to store all the dots, defined by Dot class - check in the tabs
ArrayList allConnections = new ArrayList();//same as above for connections

//a set of variables, corresponding to the sliders:
float s=10;//size of the dots, check the Dot class
float sB=0.5;//thickness of the connections, check the Bridge class
int bckg=200;//background color
int spread=10;//spread between the dots while drawing
int cSpread=15;//maximal distance for the connections, check "void connect(..)" in functions tab

void setup() {
  size(1000, 1000, P3D);
  scn=new Scene(this);
  scn.setAxesVisualHint(false);//disable cartesian axes (part of proscene library)
  initGui();//check functions tab

  for (int i=0; i<50; i++)allDots.add(new Dot(new Vec3D(random(-100, 100), random(-100, 100), random(-100, 100))));//create 50 dots and add them to allDots list
  
  smooth(32);
}

void draw() {
  background(bckg);

//here is again a usual way of taking elements from a list and doing something with them:
  for (int i=0; i<allDots.size (); i++) {//count for all the elements in allDots list
    Dot d=(Dot)allDots.get(i);//localy store the current element under the name 'd'
    d.display();//call internal functions of Dot class
  }
  //sam here for Bridges:
    for (int i=0; i<allConnections.size (); i++) {
    Bridge b=(Bridge)allConnections.get(i); 
    b.display();
  }  
  displayGUI();//check functions tab  
}





