void initGui() {//same as in previous tutorial
  gui=new ControlP5(this);
  gui.setAutoDraw(false);
  gui.addSlider("s")
    .setPosition(50, 50)
      .setRange(2, 50)
        ;
  gui.addSlider("sB")
    .setPosition(50, 70)
      .setRange(0.1, 5)
        ;
  gui.addSlider("bckg")
    .setPosition(50, 90)
      .setRange(0, 255)
        ;
  gui.addSlider("spread")
    .setPosition(50, 110)
      .setRange(2, 25)
        ;  
  gui.addSlider("cSpread")
    .setPosition(50, 130)
      .setRange(2, 55)
        ;
}

void displayGUI() {//same as previous tutorial
  hint(DISABLE_DEPTH_TEST);
  scn.beginScreenDrawing();
  gui.draw();  
  scn.endScreenDrawing();
  hint(ENABLE_DEPTH_TEST);
}

///the two following functions make sure, that camera interaction (rotation, panning) is off, while you touch the sliders
void controlEvent(ControlEvent theEvent) {  
  if (mousePressed)scn.disableMotionAgent();
}

void mouseReleased() {
  scn.enableMotionAgent();
}
/////////

void connect(Dot cd) {//this function requires a Dot as input
  for (int i=0; i<allDots.size (); i++) {//for all the dots...
    Dot d=(Dot)allDots.get(i); 
    if (cd.pos.distanceTo(d.pos)<cSpread) {//if the distance between the one from input and any of the list is smaller than cSpread value from the slider..
      allConnections.add(new Bridge(cd.pos, d.pos));//...create a new bridge between the two and add it to allConnections list!
    }
  }
}

