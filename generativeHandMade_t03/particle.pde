//"Particle" class describes what a particle object is and what it can do...

class Particle { 
  //internal variables 
  PVector p;//position vector
  PVector vel;//velocity vector
  PVector acc;//acceleration vector
  float s;//size of the particle
  float resp;//its response to acc vector, see below
  color c;//color of the particle

  Particle() {//"constructor" part describes the moment of creation of a new "Particle" object
    p=new PVector(random(width), random(height));//place it somewhere within screen space
    vel=new PVector(random(-1, 1), random(-1, 1));//give it an initial random velocity vector
    s=random(0.5, 2);// give it random size
    resp=random(0.01, 0.1);//give it a specific response to acc vector, this will result in differences in behavior regarding following the target
    c=color(random(255), 50, random(100), 30);//give it a color
  }
  void behave() {//this function updates the position vector, as a result of it the particle follows/escapes the mouse pointer
    if(!mousePressed)acc=PVector.sub(new PVector(mouseX, mouseY), p);// when the mouse is not pressed, find the difference between current position of the particle and current position of the mouse
    else{
     acc=PVector.sub(p,new PVector(mouseX, mouseY));// reverse the above.. 
    }
    acc.normalize();//check PVector methods for these commands
    acc.mult(resp);
    vel.add(acc);//change velocity by adding acceleration
    vel.limit(2);  
    p.add(vel);//change position by adding velocity
  }

  void display() {//this function describes how the particle is displayed:
    stroke(c);    
    strokeWeight(s);
    point(p.x, p.y);
  }

  void connect() {//this function draws the thin, transparent connections between particles within certain distance from each other
    stroke(0, 5);
    strokeWeight(0.5);
    for (Particle ptl : allPoints) {// for all the particles..
      if (p.dist(ptl.p)<100)line(p.x, p.y, ptl.p.x, ptl.p.y);//if there is any, closer than 100, draw a line between this particle position (p) and the other (ptl.p)
    }
  }
}