//echoechonoisenoise for "generative handmade"  @IKA. Vienna 2014-2015 
//about classes

ArrayList<Particle> allPoints = new ArrayList<Particle>();//create an empty list of Particle objects (described by Particle class) called "allPoints"

void setup() {
  size(1000, 1000);
  for (int i=0; i<100; i++) allPoints.add(new Particle());//add 100 new Particle objects to the allPoints list
  smooth(64);
}
void draw() {
  for (Particle ptl : allPoints) {//for any Particle that "allPoints" contains, call it "ptl" for the purpose of addressing it's internal functions and.. 
    ptl.behave();//.. call the "behave" function of the "Particle" 
    ptl.display();//.. call the "display" function of the "Particle" 
  }
}


void keyPressed() {// whenever a key is pressed...
  for (Particle ptl : allPoints)ptl.connect();//for any Particle that "allPoints" contains, call it "ptl" for the purpose of addressing it's internal functions and.. call the "connect" function of the "Particle" 
}